library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library cyclonev;
use cyclonev.all;

entity gc_video_if is
   port (
         cbl_key   : in  std_logic;
         vdata     : in  std_logic_vector(7 downto 0);
         clk54     : in  std_logic;
         cbl_det   : out  std_logic;
         clk_sel   : in  std_logic;
         clk_27    : in  std_logic;
         clk108    : out std_logic;
         lrck      : in  std_logic;  
         adata     : in  std_logic; 
         bclk      : in  std_logic;
         vga_vs    : out std_logic;
         vga_hs    : out std_logic;
         vga_r     : out std_logic_vector(7 downto 0);
         vga_g     : out std_logic_vector(7 downto 0);
         vga_b     : out std_logic_vector(7 downto 0);
         vga_blank : out std_logic;
         vga_clk   : out std_logic
      );
end entity gc_video_if;

architecture rtl of gc_video_if is
   
   
   signal vdata_ibuf : std_logic_vector(7 downto 0);
   signal vdata_reg : std_logic_vector(7 downto 0);
   signal sync_reg  : std_logic_vector(0 to 2); -- used to keep track of where in the stream we are :)
   signal hsync     : std_logic;
   signal vsync     : std_logic;
   signal blank     : std_logic;
   signal mode      : std_logic;
   signal field     : std_logic;
   signal hsync_p   : std_logic_vector(0 to 9);
   signal vsync_p   : std_logic_vector(0 to 9);
   signal blank_p   : std_logic_vector(0 to 9);
   
   attribute syn_noprune   : boolean;    -- Attribute to keep synthesizer from removing signals with no fan-out
   attribute syn_noprune of vdata_reg : signal is true; -- This is my new best friend ;)
   attribute syn_noprune of hsync     : signal is true;
   attribute syn_noprune of vsync     : signal is true;
   attribute syn_noprune of mode      : signal is true;
   attribute syn_noprune of sync_reg  : signal is true;
   
   -- Video pipeline signals
   type buf_array_type is array (integer range <>) of std_logic_vector(7 downto 0);
   
   -- Pixel buffers
   signal yx_buf : buf_array_type(0 to 2);
   signal cb_buf : buf_array_type(0 to 1);
   signal cr_buf : buf_array_type(0 to 1);
   signal pixel_clk : std_logic := '0'; -- maybe use clk27 instead?
   
   -- H/V position counters: should count to limit and then wrap
   signal hpos   : unsigned(10 downto 0); -- Counts from 0 - 857
   signal vpos   : unsigned(8 downto 0); -- Counts from 0 - 524
   attribute syn_noprune of hpos  : signal is true;
   
   -- Y Cb Cr signals:
   signal Y  : signed(8 downto 0);             -- Sign extended
   signal Cb : std_logic_vector(8 downto 0);   -- Sign extended
   signal Cr : std_logic_vector(8 downto 0);   -- Sign extended
   signal clk_sel_reg : std_logic_vector(0 to 2);
   
   -- Conversion coefficients
   constant c_a_y  : signed(17 downto 0) := to_signed(38155, 18);
   constant c_r_cr : signed(17 downto 0) := to_signed(52299, 18);
   constant c_g_cb : signed(17 downto 0) := to_signed(12837, 18);
   constant c_g_cr : signed(17 downto 0) := to_signed(26639, 18);
   constant c_b_cb : signed(17 downto 0) := to_signed(66101, 18);
   
   -- Multiplication results: 18+8 bits
   signal y_a_mul :  signed(26 downto 0);
   signal r_cr_mul : signed(26 downto 0);
   signal g_cb_mul : signed(26 downto 0);
   signal g_cr_mul : signed(26 downto 0);
   signal b_cb_mul : signed(26 downto 0);
   
   signal R, G, B : signed(26 downto 0);
   
   signal clk54_s : std_logic;
   
	signal clk54_180 : std_logic;
	signal clk54_0  : std_logic;
	signal clk216 : std_logic;
	attribute syn_noprune of clk216  : signal is true;
	
	--signal vga_clk_int : std_logic;
	
	signal vdata_216 : std_logic_vector(7 downto 0);
	attribute syn_noprune of vdata_216  : signal is true;
   
   --signal vdata_zero : std_logic;
	component pll is
	port (
		refclk   : in  std_logic := '0'; --  refclk.clk
		rst      : in  std_logic := '0'; --   reset.reset
		outclk_0 : out std_logic;        -- outclk0.clk
		outclk_1 : out std_logic;        -- outclk1.clk
		outclk_2 : out std_logic;        -- outclk2.clk
		locked   : out std_logic         --  locked.export
	);
	end component pll;
	
	component cyclonev_clkena
   generic 
   (
      clock_type	:	STRING := "Auto";
      disable_mode	:	STRING := "low";
      ena_register_mode	:	STRING := "always enabled";
      ena_register_power_up	:	STRING := "high";
      test_syn	:	STRING := "high";
      lpm_type	:	STRING := "cyclonev_clkena"
   );
   port
   ( 
      ena	:	IN STD_LOGIC := '1';
      enaout	:	OUT STD_LOGIC;
      inclk	:	IN STD_LOGIC := '1';
      outclk	:	OUT STD_LOGIC
   ); 
   end component;
   
      
begin
   
	i_pll : pll
	port map (
		refclk => clk54_s,
		rst      => '0',
		outclk_0 => clk54_0,
		outclk_1 => clk54_180,
		outclk_2 => clk108,
		locked   => open
	);
	vga_clk <= clk54_180;
   --vga_r <= std_logic_vector(Y(7 downto 0));
   --vga_g <= std_logic_vector(Cb(7 downto 0));
   --vga_b <= std_logic_vector(Cr(7 downto 0));
   
   clkctrl1 :  cyclonev_clkena
	  GENERIC MAP (
		clock_type => "auto",
		ena_register_mode => "none"
	  )
	  PORT MAP ( 
		ena => '1',
		inclk => clk54,
		outclk => clk54_s
	  );

--altclkctrl_0 : component clkctrl_altclkctrl_0
--		port map (
--			inclk  => clk54,  --  altclkctrl_input.inclk
--			outclk => clk54_s  -- altclkctrl_output.outclk
--		);
	  
	--vga_clk <= vga_clk_int;
   vga_vs <= vsync_p(7);
   vga_hs <= hsync_p(7);
   vga_blank <= blank_p(7);
   
   --process(clk216)
   --begin
   --   if rising_edge(clk216) then
   --      vdata_216 <= vdata;
   --   end if;
   --end process;
   --vsync_p <= vsync; 
   
   Y  <= signed('0' & yx_buf(0)) - 16;
   Cb <= not cb_buf(0)(7) & not cb_buf(0)(7) & cb_buf(0)(6 downto 0);
   Cr <= not cr_buf(0)(7) & not cr_buf(0)(7) & cr_buf(0)(6 downto 0);
   
   
   --vga_blank <= '1';
   cbl_det <= cbl_key;
   p_video:process(clk54_0)
   begin
      if rising_edge(clk54_0) then
         blank_p <= blank & blank_p(0 to 8);
         vsync_p <= vsync & vsync_p(0 to 8);
         hsync_p <= hsync & hsync_p(0 to 8);
         
         vdata_ibuf <= vdata;
         clk_sel_reg <= clk_sel & clk_sel_reg(0 to 1);
         -- Clock RGB data:
         if R(26) = '1' then    -- Negative value, round to zero
            vga_r <= (others => '0');
         elsif R(23) = '1' then -- Overflow, saturate to 255
            vga_r <= (others => '1');
         else                   -- Calculation OK
            vga_r <= std_logic_vector(R(22 downto 15));
         end if;
         
         if G(26) = '1' then -- neg
            vga_g <= (others => '0');
         elsif G(23) = '1' then -- overflow
            vga_g <= (others => '1');
         else
            vga_g <= std_logic_vector(G(22 downto 15));
         end if;
         
         if B(26) = '1' then -- neg
            vga_b <= (others => '0');
         elsif B(23) = '1' then -- overflow
            vga_b <= (others => '1');
         else
            vga_b <= std_logic_vector(B(22 downto 15));
         end if;
         
         
         
         
         vdata_reg <= vdata_ibuf;
         pixel_clk <= not pixel_clk;
         -- Shift sync register data
         sync_reg(1 to 2) <= sync_reg(0 to 1);
         if vdata_ibuf = x"00" then
            sync_reg(0) <= '1';
         else
            sync_reg(0) <= '0';
         end if;
         --vga_clk <= hpos(0);
         if vdata_reg = x"00" then
            blank <= '0';
         else
            --vga_blank <= '1';
         end if;
         
         hpos <= hpos + 1;
         if hpos(0) = '0' then
            --vga_r <= vdata_reg;
            --vga_g <= vdata_reg;
            --vga_b <= vdata_reg;
         end if;
         
         if sync_reg = "010" and vdata_ibuf /= x"00" then -- clear counter
            hpos <= (others => '0');
            blank <= '1';        
         end if;
         
         case hpos(1 downto 0) is
            when "00" => -- Y1
               yx_buf(2) <= vdata_reg;
               yx_buf(1) <= yx_buf(2);
               yx_buf(0) <= yx_buf(1); -- clock outputs
               cb_buf(0) <= cb_buf(1);
               cr_buf(0) <= cr_buf(1);
               --vga_r <= yx_buf(0);
               --vga_g <= cb_buf(0);
               --vga_b <= cr_buf(0);
            when "01" => -- Cb
               if clk_sel_reg(1) = '0' then
                  cb_buf(1) <= vdata_reg;
               else
                  cr_buf(1) <= vdata_reg;
               end if;
            when "10" => -- Y2
               yx_buf(2) <= vdata_reg;
               yx_buf(1) <= yx_buf(2);
               yx_buf(0) <= yx_buf(1);
               --Y  <= yx_buf(0);
               --Cb <= cb_buf(0);
               --Cr <= cr_buf(0);
               --Y  <= signed('0' & yx_buf(0)) - 16;
               --Cb <= not cb_buf(0)(7) & not cb_buf(0)(7) & cb_buf(0)(6 downto 0);
               --Cr <= not cr_buf(0)(7) & not cr_buf(0)(7) & cr_buf(0)(6 downto 0);
               --vga_r <= yx_buf(0);
               --vga_g <= cb_buf(0);
               --vga_b <= cr_buf(0);
            when "11" => -- Cr
               if clk_sel_reg(0) = '0' then
                  cr_buf(1) <= vdata_reg;
                  --cb_buf(0) <= cb_buf(1);
               else
                  cb_buf(1) <= vdata_reg;
                  --cr_buf(0) <= cr_buf(1);
               end if;
         end case;
         
         -- Do conversion:
         y_a_mul  <= Y * c_a_y;
         r_cr_mul <= signed(Cr) * c_r_cr;
         g_cb_mul <= signed(Cb) * c_g_cb;
         g_cr_mul <= signed(Cr) * c_g_cr;
         b_cb_mul <= signed(Cb) * c_b_cb;
         
         R <= y_a_mul + r_cr_mul;
         G <= y_a_mul - g_cb_mul - g_cr_mul;
         B <= y_a_mul + b_cb_mul;
         
         
         
         if vdata_reg = x"00" then -- get sync signals!
            hsync <= vdata_ibuf(4);
            vsync <= vdata_ibuf(5);
            if vdata_ibuf(0) = '1' then
               mode <= '1'; -- progressive
            end if;
            if vdata_ibuf = x"00" then
               mode <= '0'; -- interlaced
            end if;
            field   <= vdata_ibuf(6); -- odd/even field
         end if;
      end if;
   end process;
   
end architecture rtl;
