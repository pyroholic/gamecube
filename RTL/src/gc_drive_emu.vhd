library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gc_drive_emu is
   generic (
      FIFO_DEPTH : natural := 2048 -- Depth in bytes
   );
   port (
      clk      : in    std_logic;                     -- System input clock (50 MHz)
      reset    : in    std_logic;                     -- System reset input
      -- DVD drive interface, change to correct port before switching back
      di_hclk  : in    std_logic;                     -- GameCube host strobe/clk
      di_dclk  : out   std_logic;                     -- DVD drive device strobe/clk
      di_data  : inout std_logic_vector(7 downto 0);  -- Gamecube/DVD data bus
      di_rw    : in    std_logic;                     -- Read/Write (1 - read, 0 - write)
      di_error : out   std_logic;                     -- Error output (active ?)
      di_reset : in    std_logic;                     -- Gamecube reset request (active low)
      di_break : inout std_logic;                     -- Break/interrupt, active high, don't know how this works
      di_cover : out   std_logic;                     -- Cover status (0 - closed, 1 - open)
      -- Avalon Interface
      address  : in    std_logic_vector(3 downto 0);  -- Avalon RW Address
      read     : in    std_logic;                     -- Avalon read strobe
      rdata    : out   std_logic_vector(31 downto 0); -- Avalon read data
      write    : in    std_logic;                     -- Avalon write strobe
      wdata    : in    std_logic_vector(31 downto 0); -- Avalon write data
      bytemask : in    std_logic_vector(3 downto 0);  -- Avalon write data mask
      irq      : out   std_logic
   );
end entity gc_drive_emu;

architecture rtl of gc_drive_emu is
   type cmdbuf_type is array (0 to 11) of std_logic_vector(7 downto 0);
   type regwire_type is array (0 to 3) of std_logic_vector(31 downto 0);
   
   type fifo_mem_out_array_type is array (0 to 3) of std_logic_vector(7 downto 0);
   
   signal cmdIrq      : std_logic := '0';
   signal cmdbuf      : cmdbuf_type;
   signal cmdbuf_offs : integer range 0 to 15 := 0;
   
   signal newCmd      : std_logic := '0';
   signal addr        : std_logic_vector(3 downto 0);
   
   signal regwire     : regwire_type;
   signal regwire_buf1: regwire_type;
   signal regwire_buf2: regwire_type;
   
   signal dclk        : std_logic;
   
   signal sync        : std_logic_vector(0 to 2);
   
   -- Sync registers
   signal di_hclk_s  : std_logic_vector(0 to 2);
   signal di_data_s0 : std_logic_vector(7 downto 0);
   signal di_data_s1 : std_logic_vector(7 downto 0);
   signal di_rw_s    : std_logic_vector(0 to 1);
   signal di_reset_s : std_logic_vector(0 to 1);
   
   
   attribute syn_noprune   : boolean;    -- Attribute to keep synthesizer from removing signals with no fan-out
   attribute syn_noprune of cmdbuf : signal is true; -- This is my new best friend ;)
   attribute syn_noprune of cmdbuf_offs : signal is true; -- This is my new best friend ;)
   attribute syn_noprune of newCmd : signal is true; -- This is my new best friend ;)
   attribute syn_noprune of dclk : signal is true; -- This is my new best friend ;)
   attribute syn_noprune of sync : signal is true; -- This is my new best friend ;)
   attribute syn_noprune of regwire : signal is true; -- This is my new best friend ;)
   
   
   type fifo_flags_type is record
      empty    : std_logic;
      full     : std_logic;
      free     : integer range 0 to FIFO_DEPTH-1; -- replace with fifo depth
   end record fifo_flags_type;
   
   type fifo_type is record
      readpos  : integer range 0 to FIFO_DEPTH-1;
      writepos : integer range 0 to (FIFO_DEPTH/4)-1;
   end record fifo_type;
   
   signal fifo       : fifo_type;
   signal fifo_flags : fifo_flags_type;
   
   type fifo_mem_type is array (0 to FIFO_DEPTH-1) of std_logic_vector(31 downto 0);
   signal fifo_mem : fifo_mem_type;
   signal fifo_mem_out : std_logic_vector(31 downto 0);
   signal fifo_mem_out_array : fifo_mem_out_array_type;
   
   type di_reg_type is record
      data       : std_logic_vector(7 downto 0);
      dclk       : std_logic;
      lid        : std_logic;
      errb       : std_logic;
      readFifo   : std_logic;
      toggleClk  : integer range 0 to 5;--2; 0 1 2 0 1 2 0 1 2 3 4 5
      tComplete  : std_logic; -- transfer from ISO file to FIFO done
   end record;
   
   signal di_reg : di_reg_type;
   
   attribute syn_noprune of di_reg : signal is true; -- This is my new best friend ;)
   attribute syn_noprune of fifo_flags : signal is true;
   
begin
   
   
   --(others => '0'); -- put flags here later
   
   addr <= address(3 downto 2) & "00";
   
   di_data  <= (others => 'Z') when di_rw_s(1) = '0' else -- use the syncronized signal here instead!!
               di_reg.data;--)
   di_dclk  <= di_reg.dclk;
   di_error <= '1'; -- no error
   di_cover <= '0'; -- cover closed
   di_break <= 'Z';
   
   irq      <= cmdIrq;
      
   -- FIFO output
   fifo_mem_out <= fifo_mem(fifo.readpos/4);
   fifo_mem_out_array(3) <= fifo_mem_out(31 downto 24);
   fifo_mem_out_array(2) <= fifo_mem_out(23 downto 16);
   fifo_mem_out_array(1) <= fifo_mem_out(15 downto 8);
   fifo_mem_out_array(0) <= fifo_mem_out(7 downto 0);
   
   p_fifo_flags:process(fifo)
   begin
      -- FIFO empty flag
      if (fifo.readpos/4) = fifo.writepos then
         fifo_flags.empty <= '1';
      else
         fifo_flags.empty <= '0';
      end if;
      
      -- FIFO full flag
      if (fifo.readpos/4) < fifo.writepos then
         -- FIFO readpos behind writepos, FIFO full when readpos = 0 and writepos = last entry
         if (fifo.readpos/4) = 0 and fifo.writepos = ((FIFO_DEPTH/4) - 1) then
            fifo_flags.full <= '1';
         else
            fifo_flags.full <= '0';
         end if;            
      else 
         -- FIFO readpos ahead of writepos, FIFO full when writepos = readpos - 1
         if fifo.writepos = ((fifo.readpos/4) - 1) then
            fifo_flags.full <= '1';
         else
            fifo_flags.full <= '0';
         end if;
      end if;
      
      -- FIFO free entries
      if (fifo.readpos/4) <= fifo.writepos then
         -- FIFO readpos behind writepos
         fifo_flags.free <= (FIFO_DEPTH-1) - (fifo.writepos*4) + fifo.readpos;
      else
         -- FIFO readpos ahead of writepos
         fifo_flags.free <= fifo.readpos - (fifo.writepos*4) - 1;
      end if;
   end process;
   
   p_fifo:process(clk)
   begin
      if rising_edge(clk) then
         -- Buffer registers
         regwire_buf1(0) <= cmdbuf(0) & cmdbuf(1) & cmdbuf(2) & cmdbuf(3);
         regwire_buf1(1) <= cmdbuf(4) & cmdbuf(5) & cmdbuf(6) & cmdbuf(7);
         regwire_buf1(2) <= cmdbuf(8) & cmdbuf(9) & cmdbuf(10) & cmdbuf(11);
         regwire_buf1(3) <= std_logic_vector(to_unsigned(fifo_flags.free, 32));
         regwire_buf2    <= regwire_buf1;
         regwire         <= regwire_buf2;
      
         -- FIFO read/write logic
         if write = '1' and address = "0000" and fifo_flags.full = '0' then -- Write FIFO
            fifo_mem(fifo.writepos) <= wdata;
            if fifo.writepos < (FIFO_DEPTH/4)-1 then
               fifo.writepos <= fifo.writepos + 1;
            else
               fifo.writepos <= 0;
            end if;
         end if;
         
         if reset = '1' then
            fifo.readpos  <= 0;
            fifo.writepos <= 0;
         end if;
         
         if di_reg.readFifo = '1' and fifo_flags.empty = '0' then -- hijack for testing
            if fifo.readpos < FIFO_DEPTH-1 then
               fifo.readpos <= fifo.readpos + 1;
            else 
               fifo.readpos <= 0;
            end if;
         end if;
      end if;
   
   end process p_fifo;
   
   
   p_hps:process(clk)
   begin
      if rising_edge(clk) then
         -- Set default for single-cycle signals
         di_reg.readFifo <= '0';
         
         sync <= newCmd & sync(0 to 1);
         
         -- Create clock divider for toggling clock
         if di_reg.toggleClk < 3 then
            di_reg.toggleClk <= di_reg.toggleClk + 1;
         else
            di_reg.toggleClk <= 0;
         end if;
         
         -- Check for new command:
         if sync(1 to 2) = "10" then -- New command written, set dclk
            di_reg.dclk      <= '1'; -- Acknowledge request
            di_reg.tComplete <= '0'; -- Clear transfer complete flag
         end if;
         -- Check fifo status to see if we are ready to start transmitting data
         if di_reg.toggleClk = 0 then -- maybe add check here to make sure that GC is ready to receive.
            if fifo_flags.empty = '0' and di_reg.dclk = '1' then -- The FIFO contains data to transmit
               di_reg.data     <= fifo_mem_out_array(fifo.readpos mod 4);
               di_reg.dclk     <= '0';
               --di_reg.readFifo <= '1';
            elsif di_reg.dclk = '0' and (di_reg.tComplete = '0' or fifo_flags.empty = '0') then
               -- Create a rising edge if data has been clocked to output register
               di_reg.dclk     <= '1';
               di_reg.readFifo <= '1'; -- Maybe need to check this, but should be okay since we wont 
            elsif di_reg.tComplete = '1' and fifo_flags.empty = '1' then
               di_reg.dclk     <= '0';
            end if;
         end if;
         
         -- Status register writes from HPS
         if write = '1' and address = "0100" then -- Transfer complete
            di_reg.tComplete <= '1';
         end if;
         
         if sync(1) = '1' then
            dclk <= '1';
         else
            dclk <= '0';
         end if;
         
         if sync(1 to 2) = "10" then -- Set IRQ on new write command
            cmdIrq <= '1';
         end if;
         
         if read = '1' then
            rdata <= regwire(to_integer(unsigned(addr(3 downto 2))));
            if address = "0000" then -- 
            	cmdIrq <= '0'; -- clear IRQ-flag on command read
            end if;
         end if;
         
         if reset = '1' then
            di_reg.data      <= (others => '0');
            di_reg.dclk      <= '1'; -- check the default!! think it is zero
            di_reg.lid       <= '0'; -- lid closed
            di_reg.errb      <= '1'; -- no error!
            di_reg.readFifo  <= '0'; -- idle
            di_reg.toggleClk <= 0; 
            di_reg.tComplete <= '0';
            cmdIrq <= '0';
            dclk   <= '0';
            sync   <= (others => '0');
         end if;         
         
      end if;
   end process;
   
   p_sync:process(clk)
   begin
      if rising_edge(clk) then
         -- Synchronize input signals to internal clock domain
         di_hclk_s  <= di_hclk & di_hclk_s(0 to 1);
         di_data_s1 <= di_data_s0;
         di_data_s0 <= di_data;
         di_rw_s    <= di_rw & di_rw_s(0);
         di_reset_s <= di_reset & di_reset_s(0);
      end if;
   end process;
   
   
   --p_cmd:process(di_hclk, di_reset, di_rw)
   p_cmd:process(clk, di_reset, di_rw)
   begin
      if rising_edge(clk) then
      if di_hclk_s(1 to 2) = "10" then -- rising edge
         if di_rw_s(1) = '0' then -- only write data when rw = '0'
         if cmdbuf_offs < 12 then
         --   cmdbuf(cmdbuf_offs) <= di_data;
            -- Try new solution:
   -------------------------------------------
            cmdbuf(11) <= di_data_s1;
            l_cmd:for i in 0 to 10 loop
               cmdbuf(i) <= cmdbuf(i+1);
            end loop;
   -------------------------------------------
            cmdbuf_offs <= cmdbuf_offs + 1;
         else
            cmdbuf_offs <= 15; -- error!
         end if;
         end if;
         
         if cmdbuf_offs = 8 then
            newCmd <= '1';
         end if;
      end if;
      end if;
      
      if di_reset_s(1) = '0' or di_rw_s(1) = '1' then
         cmdbuf_offs <= 0;
         newCmd <= '0';
      end if;
      
      
   end process p_cmd;
   

end architecture rtl;
