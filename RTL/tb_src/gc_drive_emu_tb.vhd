library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library gc_dev_lib;

entity gc_drive_emu_tb is
end entity gc_drive_emu_tb;

architecture behaviorial of gc_drive_emu_tb is
   signal clk      : std_logic := '0';
   signal reset    : std_logic := '1';
      -- DVD drive interface, change to correct port before switching back
   signal di_hclk  : std_logic := '0';
   signal di_dclk  : std_logic;
   signal di_data  : std_logic_vector(7 downto 0);
   signal di_rw    : std_logic := '0';
   signal di_error : std_logic;
   signal di_reset : std_logic;
   signal di_break : std_logic;
   signal di_cover : std_logic;
      -- Avalon Interface
   signal address  : std_logic_vector(3 downto 0) := "0000";
   signal read     : std_logic := '0';
   signal rdata    : std_logic_vector(31 downto 0) := (others => '0');
   signal write    : std_logic := '0';
   signal wdata    : std_logic_vector(31 downto 0) := (others => '0');
   signal bytemask : std_logic_vector(3 downto 0) := "0000";
   signal irq      : std_logic;
begin
   
   gc_drive_emu0 : entity gc_dev_lib.gc_drive_emu
   port map (
      clk      => clk,
      reset    => reset,
      -- DVD drive interface, change to correct port before switching back
      di_hclk  => di_hclk,
      di_dclk  => di_dclk,
      di_data  => di_data,
      di_rw    => di_rw,
      di_error => di_error,
      di_reset => di_reset,
      di_break => di_break,
      di_cover => di_cover,
      -- Avalon Interface
      address  => address,
      read     => read,
      rdata    => rdata,
      write    => write,
      wdata    => wdata,
      bytemask => bytemask,
      irq      => irq
   );
   
   p_clk:process
   begin
      wait for 1 ns;
      clk <= not clk;
   end process;
   
   p_reset:process
   begin
      wait for 10 ns;
      reset <= '0';
      wait ;
   end process;
   
   p_rw:process
   begin
      wait for 200 ns;
      write   <= '1';
      address <= "0000";
      wdata   <= x"04030201";
      wait for 2 ns;
      wdata   <= x"08070605";
      wait for 2 ns;
      wdata   <= x"efbeadde";
      wait for 6 ns;
      wdata   <= x"fefefefe";
      wait for 2 ns;
      write   <= '0';
      wait for 300 ns;
      write   <= '1';
      wdata   <= x"aabbccdd";
      wait for 2 ns;
      write   <= '0';
      wait for 100 ns;
      write   <= '1';
      address <= "0100";
      wait for 2 ns;
      write   <= '0';
      wait ;
      
   end process;
   
   p_read_request:process
   begin
      wait for 20 ns;
      di_rw <= '0';
      wait for 4 ns;
      di_hclk <= '1';
      wait for 4 ns;
      di_hclk <= '0';
      wait for 4 ns;
      di_hclk <= '1';
      wait for 4 ns;
      di_hclk <= '0';
      wait for 4 ns;
      di_hclk <= '1';
      wait for 4 ns;
      di_hclk <= '0';
      wait for 4 ns;
      di_hclk <= '1';
      wait for 4 ns;
      di_hclk <= '0';
      wait for 4 ns;
      di_hclk <= '1';
      wait for 4 ns;
      di_hclk <= '0';
      wait for 4 ns;
      di_hclk <= '1';
      wait for 4 ns;
      di_hclk <= '0';
      wait for 4 ns;
      di_hclk <= '1';
      wait for 4 ns;
      di_hclk <= '0';
      wait for 4 ns;
      di_hclk <= '1';
      wait for 4 ns;
      di_hclk <= '0';
      wait for 4 ns;
      di_hclk <= '1';
      wait for 4 ns;
      di_hclk <= '0';
      wait for 4 ns;
      di_hclk <= '1';
      wait for 4 ns;
      di_hclk <= '0';
      wait for 4 ns;
      di_hclk <= '1';
      wait for 4 ns;
      di_hclk <= '0';
      wait for 4 ns;
      di_hclk <= '1';
      wait for 16 ns;
      di_hclk <= '0';
      wait ;
   end process;
   
end architecture behaviorial;
