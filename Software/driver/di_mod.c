#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/ioport.h>
#include <linux/io.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/semaphore.h>
#include <linux/interrupt.h>
#include <linux/spinlock_types.h>
#include <linux/uaccess.h>

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h> /*this is the file structure, file open read close */
#include <linux/cdev.h> /* this is for character device, makes cdev avilable*/
#include <linux/semaphore.h> /* this is for the semaphore*/
#include <linux/uaccess.h> /*this is for copy_user vice vers*/
#include <linux/errno.h>
#include <linux/memory.h>

#include <linux/slab.h>
#include "di_mod.h"

MODULE_LICENSE("GPL");

#define PRINT_DBG_INFO 0

#define GCMEM_BASE 0xc0000000
#define GCMEM_SIZE PAGE_SIZE
#define GC_INT_NUM 72

#define DEVICE_NAME "gc_dvd"


void *gc_dvd_mem;

struct semaphore sem;

int ret; /*used to return values*/

struct class *cl;
struct cdev gc_cdev; /*this is the name of my char driver that i will be registering*/
dev_t dev_num; /*will hold the major number that the kernel gives*/

static DEFINE_SEMAPHORE(interrupt_mutex);
static DECLARE_WAIT_QUEUE_HEAD(wq);
static DEFINE_SPINLOCK(interrupt_flag_lock);

static int newCmd_flag = 0;

int gc_dvd_init(void);
void gc_dvd_exit(void);
static int gc_dvd_open(struct inode *, struct file *);
static int gc_dvd_close(struct inode *, struct file *);
static ssize_t gc_dvd_read(struct file *, char *, size_t, loff_t *);
static ssize_t gc_dvd_write(struct file *, const char *, size_t, loff_t *);

static struct {
   int cmd;
   int offset;
   int length;
} cmdBuf;


static irqreturn_t gc_dvd_interrupt(int irq, void *dev_id)
{
	if (irq != GC_INT_NUM)
		return IRQ_NONE;
#if PRINT_DBG_INFO
	printk("Command received\n");
#endif
	spin_lock(&interrupt_flag_lock);
	cmdBuf.cmd = ioread32(gc_dvd_mem);
	cmdBuf.offset = ioread32(gc_dvd_mem+4);
	cmdBuf.length = ioread32(gc_dvd_mem+8);
#if PRINT_DBG_INFO
	printk("CMD: 0x%08x\n", cmdBuf.cmd);
	printk("Offs: 0x%08x\n", cmdBuf.offset);
	printk("Size: 0x%08x\n", cmdBuf.length);
#endif
	newCmd_flag = 1;
	spin_unlock(&interrupt_flag_lock);

	wake_up_interruptible(&wq);

	return IRQ_HANDLED;
}

static int gc_dvd_open(struct inode *inode, struct file *fp) {
	return 0;
}

static int gc_dvd_close(struct inode *inode, struct file *fp) {
#if PRINT_DBG_INFO
	printk("Device closed");
#endif
	return 0;
}

static ssize_t gc_dvd_read(struct file *fp, char *buf, size_t length, loff_t *offset) {
	int ret;
	//printk("Trying to read, going to sleep\n");
	if (wait_event_interruptible(wq, newCmd_flag != 0)) {
		return -ERESTART;
	}
	//printk("Process woken by interrupt\n");
	//printk("requested bytes: %d", length);
	ret = copy_to_user(buf, &cmdBuf, 12);
	//printk("Remaining bytes %d", ret);
	spin_lock(&interrupt_flag_lock);
	newCmd_flag = 0;
	spin_unlock(&interrupt_flag_lock);

	return 12; // hard coded to transfer only 12 bytes / read
}
static ssize_t gc_dvd_write(struct file *fp, const char *buf, size_t length, loff_t *offset) {
	register int i;
	register int tmp;
#if PRINT_DBG_INFO
	printk("Received %d bytes of data\n", length);
#endif
	tmp = ioread32(gc_dvd_mem+0xc);
	while (tmp < 2000) {
		tmp = ioread32(gc_dvd_mem+0xc);
	}
	for (i=0;i<length/4;i++) {
		//get_user(tmp, (int*)buf+i);
		tmp = *((int __user *)buf+i);
		//iowrite32(tmp, gc_dvd_mem);
		iowrite32(tmp, gc_dvd_mem);
	}
	return 0;
}

long gc_dvd_ioctl(struct file *file, unsigned int ioctl_num, unsigned long ioctl_param)
{
	long ret = 0;
	int tmp;
	/*printk("IOCTL received, number 0x%08x\n", ioctl_num);
	printk("IOCTL GCDVD_READ_FIFO_STATUS = 0x%08x\n", GCDVD_READ_FIFO_STATUS);
	printk("IOCTL PARAM = 0x%08x\n", (int)ioctl_param);
	printk("IOCTL FILE = 0x%08x\n", (int)file);*/
	/*
	 * Switch according to the ioctl called
	 */
	switch (ioctl_num) {
	case GCDVD_READ_FIFO_STATUS:
		printk("Reading FIFO status register\n");
		ret = ioread32(gc_dvd_mem+0xc);
		//temp = (char *)ioctl_param;
		//get_user(ch, temp);

		break;

	case GCDVD_SET_TCOMPLETE:
		tmp = ioread32(gc_dvd_mem+0xc);
		while(tmp < 2046) {
			tmp = ioread32(gc_dvd_mem+0xc);
		}
		iowrite32(0, gc_dvd_mem+0x4);
		//printk("Finishing transaction\n");
		//put_user('\0', (char *)ioctl_param + i);
		break;

	case GCDVD_SET_FLAGS:
		iowrite32(ioctl_param, gc_dvd_mem+0x8);
		break;
	}
	return ret;//SUCCESS;
}

struct file_operations fops = {
		.owner = THIS_MODULE,
		.open = gc_dvd_open,
		.unlocked_ioctl = gc_dvd_ioctl,
		.write = gc_dvd_write,
		.read = gc_dvd_read,
		.release = gc_dvd_close,

};

int gc_dvd_init(void) {

	struct resource *res;
#if PRINT_DBG_INFO
	printk(KERN_INFO "Initializing gc_dvd driver\n");
#endif
	//int alloc_chrdev_region(dev_t *dev, unsigned int firstminor, unsigned int count, char *name);
	if (alloc_chrdev_region(&dev_num, 0, 1, DEVICE_NAME) < 0) {
		return -1;
	}
	if ((cl = class_create(THIS_MODULE, "gc_dvd")) == NULL) {
		unregister_chrdev_region(dev_num, 1);
		return -1;
	}
	if (device_create(cl, NULL, dev_num, NULL, DEVICE_NAME) == NULL) {
		class_destroy(cl);
		unregister_chrdev_region(dev_num, 1);
		return -1;
	}
	cdev_init(&gc_cdev, &fops);
	if (cdev_add(&gc_cdev, dev_num, 1) == -1) {
		device_destroy(cl, dev_num);
		class_destroy(cl);
		unregister_chrdev_region(dev_num, 1);
		return -1;
	}
#if PRINT_DBG_INFO
	printk(KERN_INFO "Done!\n");
#endif

	res = request_mem_region(GCMEM_BASE, GCMEM_SIZE, "gc_dvd");
	if (res == NULL) {
		ret = -EBUSY;
	}

	gc_dvd_mem = ioremap(GCMEM_BASE, GCMEM_SIZE);
	if (gc_dvd_mem == NULL) {
		ret = -EFAULT;
	}

	ret = request_irq(GC_INT_NUM, gc_dvd_interrupt,	0, "gc_dvd", NULL);
	if (ret < 0) {

	}

	return 0;
}

void gc_dvd_exit(void)
{
#if PRINT_DBG_INFO
	printk(KERN_INFO "Unloading gc_dvd driver\n");
#endif
	free_irq(GC_INT_NUM, NULL);
	iounmap(gc_dvd_mem);
	release_mem_region(GCMEM_BASE, GCMEM_SIZE);
	cdev_del(&gc_cdev);
	device_destroy(cl, dev_num);
	class_destroy(cl);
	unregister_chrdev_region(dev_num, 1);
#if PRINT_DBG_INFO
	printk(KERN_INFO "Done\n");
#endif
}

module_init(gc_dvd_init);
module_exit(gc_dvd_exit);
