#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>

#include "../driver/di_mod.h"

#define DVD_INQUIRY	      0x12000000
#define DVD_READ_SECTOR    0xa8000000
#define DVD_INIT           0xa8000040
#define DVD_SEEK           0xab000000
#define DVD_REQUEST_ESTAT  0xe0000000
#define DVD_PLAY_STREAM    0xe1000000
#define DVD_REQEUST_ASTAT  0xe2000000
#define DVD_STOP_MOTOR     0xe3000000
#define DVD_DISABLE_AUDIO  0xe4000000
#define DVD_ENABLE_AUDIO   0xe4010000

const char drive_info[32] = {
		0x00,0x00,0x00,0x00,
		0x20,0x01,0x06,0x08,
		0x61,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00
};

int drive_resp;
char *drive_resp_ptr = (char *)&drive_resp;


int main(int argc, char *argv[]) {
	// Command struct
	struct {
		int cmd;
		int offset;
		int length;
	} cmdBuf;

	int fd;
	char buf[2048];
	int ret;
	int first_read;
	//FILE *iso;
	int iso;
	int offset;
	int patch;
	int patch_size;
	int trailer_size;
	char * trailer_size_ptr = (char*)&trailer_size;
	int *calc_ptr;
	int addr_calc;
	int injection_offset = 0;
	char * addr_calc_ptr = (char *)&addr_calc;
	
	int apply_patch;

	int apploader_size;
	char * apploader_size_ptr = (char *)&apploader_size;

	char patch_buf[2048];
	struct stat patch_stat;
	printf("Opening device\n");
	fd = open("/dev/gc_dvd", O_RDWR);

	patch = open("/home/IPLBoot.bin", O_RDONLY);
	fstat(patch, &patch_stat);

	patch_size = (int)patch_stat.st_size;
	printf("Patch size: %d bytes\n", patch_size);
	read(patch, patch_buf, patch_size);
	//iso = open("/mnt/s3-pmario.iso", O_RDONLY);
	//iso = open("/mnt/mp.iso", O_RDONLY);
	if (argc != 2) {
		printf("Invalid command, usage:\nservice game.iso\n");
		return 0;
	}
	iso = open(argv[1], O_RDONLY); // open iso
	
	lseek(iso, 0x440, SEEK_SET);
	read(iso, buf, 0x20);
	if (buf[0x1b] == 0x2) {
	   apply_patch = 0;
   }
	else {
	   apply_patch = '1';
	}
	//iso = open("/mnt/GLME01.gcm", O_RDONLY);
	// To navigate in iso-file, use lseek(iso, SEEK_SET, offset);
	// That will place the stream at byte offset.

	if (!fd) {
		perror("fopen");
		return EXIT_FAILURE;
	}
	ioctl(fd, GCDVD_SET_TCOMPLETE);
	while(1) {
		read(fd, &cmdBuf, 12); // Read 12 bytes command
		/*printf("Cmd received: 0x%08x\n", cmdBuf.cmd);
		printf("DMA offset:   0x%08x\n", cmdBuf.offset);
		printf("DMA length:   0x%08x\n", cmdBuf.length);*/
		printf("cmd: 0x%08x, 0x%08x, 0x%08x\n", cmdBuf.cmd, cmdBuf.offset, cmdBuf.length);

		first_read = 1;
		switch(cmdBuf.cmd) {
		case DVD_INIT:
			//printf("Seeking to sector2 0x%08x\n", cmdBuf.offset*4);
			/*if ((ret = lseek(iso, 0, SEEK_SET))) // seeking fails for some reason...
				printf("Seek failed, 0x%08x\n", ret);*/
			lseek(iso, 0, SEEK_SET);
			read(iso, buf, 0x20);
         //buf[3] = 0x50;
			write(fd, buf, 32);
			ioctl(fd, GCDVD_SET_TCOMPLETE);
			break;
		case DVD_READ_SECTOR:
			offset = cmdBuf.offset*4;
			//printf("Seeking to sector 0x%08x\n", offset);
			/*if ((ret = lseek(iso, offset, SEEK_SET)))
				printf("Seek failed, 0x%08x\n", ret);*/
			lseek(iso, offset, SEEK_SET);

			while(cmdBuf.length != 0) {
				if (cmdBuf.length > 2048) {
					cmdBuf.length -= 2048;
					read(iso,buf, 2048);
					/*if (first_read && offset == 0x440) {
						printf("Patching country code");
						//buf[0x1b] = 0x02; // patch to pal
					}*/
					/*if (first_read && offset == 0x2460) {
						/*int i;
						printf("Patching apploader base\n");
						for (i=0x20;i<patch_size;i++) {
							//buf[i-0x20] = patch_buf[i];
						}
					}*/
					first_read = 0;
					write(fd, buf, 2048);
				} else {
					read(iso, buf, cmdBuf.length);
					// Ignore this later...
					/*if (first_read && offset == 0x440) {
						printf("Patching country code");
						//buf[0x1b] = 0x02; // patch to pal
					}*/
					// Insert code into apploader
					if(offset == 0x2460 && apply_patch) {
						// Figure out offset in current buffer
						while(injection_offset > 2048) {
							injection_offset -= 2048;
						}
						int i;
						// Inject patch
						for (i=0;i<patch_size;i++) {
							buf[injection_offset+i] = patch_buf[i];
						}
					}

					// Patch apploader header
					if (first_read && offset == 0x2440 && apply_patch) {
						int i;
						calc_ptr = (int *)(buf+0x10);
						// Get apploader size and entrypoint, store original entrypoint in patched apploader
						for (i=0;i<4;i++) {
							apploader_size_ptr[i] = buf[0x17-i];
							addr_calc_ptr[i] = buf[0x13-i];
							trailer_size_ptr[i] = buf[0x1b-i];
							patch_buf[i+0x8] = buf[0x10+i];
						}
						/*addr_calc_ptr[0] = buf[0x13];
						addr_calc_ptr[1] = buf[0x12];
						addr_calc_ptr[2] = buf[0x11];
						addr_calc_ptr[3] = buf[0x10];*/
						injection_offset = apploader_size - 0x20;
						printf("Patching apploader header, old entrypoint: 0x%08x\n", addr_calc);
						printf("Apploader size: 0x%08x\n", apploader_size);
						printf("Trailer size:   0x%08x\n", trailer_size);
						addr_calc = apploader_size+0x81200000-0x20;
						trailer_size -= apploader_size;
						apploader_size += patch_size;
						printf("New entrypoint: 0x%08x\n", addr_calc);
						printf("New apploader size: 0x%08x\n", apploader_size);
						printf("New trailer size: 0x%08x\n", trailer_size);
						for (i=0;i<4;i++) {
							buf[0x13-i] = addr_calc_ptr[i]; // copy back new entry point
							buf[0x17-i] = apploader_size_ptr[i]; // copy back new apploader size
							buf[0x1b-i] = trailer_size_ptr[i]; // copy back new trailer size
						}

					}
					first_read = 0;
					write(fd, buf, cmdBuf.length);
					cmdBuf.length = 0;

				}
			}
			ioctl(fd, GCDVD_SET_TCOMPLETE);
			break;
		case DVD_INQUIRY:
			write(fd, drive_info, 0x20);
			ioctl(fd, GCDVD_SET_TCOMPLETE);
			break;
		case DVD_REQUEST_ESTAT:
			drive_resp = 0;
			write(fd, drive_resp_ptr, 0x4);
			ioctl(fd, GCDVD_SET_TCOMPLETE);
			break;
		case DVD_SEEK:
			lseek(iso, cmdBuf.offset*4, SEEK_SET);
			ioctl(fd, GCDVD_SET_TCOMPLETE);
			break;
		default:
			printf("Error!!\n\nError!\n\n");
			drive_resp = 0;
			write(fd, drive_resp_ptr, 0x4);
			ioctl(fd, GCDVD_SET_TCOMPLETE);
			break;
		}
	}
	close(fd);

	return 0;
}
